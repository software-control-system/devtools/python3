# Python 3.6.13

Pipeline to build Python 3.6.13 on CentOS Docker image with Conan

Allows to build Python 3.6.13 for:
* CentOS 6.10 64 bits: [python-3.6.13-linux-x86_64.zip](https://gitlab.synchrotron-soleil.fr/software-control-system/devtools/python3/-/jobs/artifacts/3.6.13/download?job=linux-x86_64)
* CentOS 6.10 32 bits: [python-3.6.13-linux-i686.zip](https://gitlab.synchrotron-soleil.fr/software-control-system/devtools/python3/-/jobs/artifacts/3.6.13/download?job=linux-i686)
