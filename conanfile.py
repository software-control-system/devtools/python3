from conan import ConanFile
from conan.tools.system import package_manager
from conan.errors import ConanInvalidConfiguration
from conans import AutoToolsBuildEnvironment, tools
import os

class PythonConan(ConanFile):
    name = "python"
    version = "3.6.13"

    settings = "os", "compiler", "build_type", "arch"
    
    requires =  "openssl/1.1.1s", \
                "zlib/1.2.13", \
                "bzip2/1.0.8", \
                "sqlite3/3.39.4", \
                "libdb/5.3.28", \
                "expat/2.5.0", \
                "libpcap/1.10.1", \
                "xz_utils/5.2.5", \
                "pcre/8.45", \
                "libffi/3.4.3", \
                "libuuid/1.0.3",              

    _autotools = None

    def validate(self):
        if self.settings.os not in ["Linux"]:
            raise ConanInvalidConfiguration("This recipe supports only Linux")

    # Install devel package when unable to build correctly from conan
    def system_requirements(self):
        yum = package_manager.Yum(self)
        yum.install(["readline-devel", "ncurses-devel", "gdbm-devel", "tk-devel"])

    def source(self):
        self.run("curl -s -L -O https://www.python.org/ftp/python/%s/Python-%s.tgz" % (self.version, self.version))
        self.run("tar -xzf Python-%s.tgz --strip 1" % self.version)

    def _build_configure(self):
        if self._autotools:
            return self._autotools

        self._autotools = AutoToolsBuildEnvironment(self)
        
        self._autotools.include_paths.append(os.path.join(self.deps_cpp_info["openssl"].rootpath, "include"))
        self._autotools.library_paths.append(os.path.join(self.deps_cpp_info["openssl"].rootpath, "lib"))

        args=[
            "--enable-optimizations", 
            "--enable-shared", 
            "--with-ssl-default-suites=openssl", 
            "--with-openssl=%s" % self.deps_cpp_info["openssl"].rootpath
        ]

        self._autotools.configure(args=args)
        return self._autotools

        
    def build(self):
        env_build = self._build_configure()
        env_build.make()

    def package(self):
        env_build = self._build_configure()
        env_build.install()
        os.symlink('python3', os.path.join(self.package_folder, 'bin', 'python'))
        os.symlink('pip3', os.path.join(self.package_folder, 'bin', 'pip'))

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)
        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
    
    def deploy(self):
        self.copy("*", symlinks=True) 
